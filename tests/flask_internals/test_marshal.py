from marshmallow import Schema, fields
from unittest.mock import patch
from defiant.flask_internals.marshal import marshal_with_embedded, marshal_with


class _SimpleSchema(Schema):
    name = fields.Str(required=True)


class _ComplexSchema(Schema):
    name = fields.Str(required=True)
    simple = fields.Nested(_SimpleSchema, many=True)


class TestMarshal(object):

    _simple_schema = _SimpleSchema(strict=True)
    _complex_schema = _ComplexSchema(strict=True)

    @patch('defiant.flask_internals.marshal.request')
    def test_marshal_with_success(self, patched_request):
        patched_request.get_json.return_value = {"name": "bleh"}
        @marshal_with(self._simple_schema)
        def marshaled_func(self, schema):
            return schema

        result = marshaled_func(None)
        assert result['name'] == "bleh"

    @patch('defiant.flask_internals.marshal.request')
    def test_marshal_with_no_data(self, patched_request):
        patched_request.get_json.return_value = None
        @marshal_with(self._simple_schema)
        def marshaled_func(self, schema):
            return schema

        result = marshaled_func(None)
        assert result == {"error": "WTF no data"}

    @patch('defiant.flask_internals.marshal.request')
    def test_marshal_with_wrong_data(self, patched_request):
        patched_request.get_json.return_value = {"value": "name"}

        @marshal_with(self._simple_schema)
        def marshaled_func(self, schema):
            return schema

        result = marshaled_func(None)
        assert result == {'name': ['Missing data for required field.']}

    @patch('defiant.flask_internals.marshal.request')
    def test_marshal_with_embedded_success(self, patched_request):
        patched_request.get_json.return_value = {"name": "bleh", "simple": [{"name": "blah"}]}

        @marshal_with_embedded(self._complex_schema, _SimpleSchema(many=True, strict=True), "simple")
        def marshaled_func(self, schema):
            return schema

        result = marshaled_func(None)
        assert result['name'] == "bleh"
        assert type(result['simple']) is list

    @patch('defiant.flask_internals.marshal.request')
    def test_marshal_with_embedded_no_data(self, patched_request):
        patched_request.get_json.return_value = None

        @marshal_with_embedded(self._complex_schema, _SimpleSchema(many=True, strict=True), "simple")
        def marshaled_func(self, schema):
            return schema

        result = marshaled_func(None)
        assert result == {"error": "WTF no data"}

    @patch('defiant.flask_internals.marshal.request')
    def test_marshal_with_embedded_wrong_data(self, patched_request):
        patched_request.get_json.return_value = {"val": "name", "simple": [{"name": "blah"}]}

        @marshal_with_embedded(self._complex_schema, _SimpleSchema(many=True, strict=True), "simple")
        def marshaled_func(self, schema):
            return schema

        result = marshaled_func(None)
        assert result == {'name': ['Missing data for required field.']}

