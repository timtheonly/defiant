from tests import BaseApplicationTest


class TestHeaderResource(BaseApplicationTest):
    def test_create_header(self, app):
        """
        no headers in DB
        create header
        header in DB
        """
        response = app.get('/header')
        assert response.status_code == 200
        assert type(response.json) is list
        assert len(response.json) == 0

        response = app.post('/header', json={'name': 'x-requested-with', 'value': 'com.yarr.blah'})
        assert response.status_code == 200

        created_header = response.json
        created_header_keys = created_header.keys()
        assert type(created_header) is dict
        for key in ["id", "name", "value"]:
            assert key in created_header_keys

        response = app.get('/header')
        assert response.status_code == 200
        assert type(response.json) is list
        assert len(response.json) == 1
        assert response.json[0] == created_header

