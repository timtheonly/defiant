from tests import BaseApplicationTest


class TestBlocklistResource(BaseApplicationTest):
    def test_create_empty_blocklist(self, app):
        """
        no blocklist in DB
        create empty blocklist
        blocklist in DB
        :param app: fixture flask testing_client
        :return: None
        """

        response = app.get('/blocklist')
        assert response.status_code == 200
        assert type(response.json) is list
        assert len(response.json) == 0

        response = app.post('/blocklist', json={'name': 'blah'})
        assert response.status_code == 200

        created_blocklist = response.json
        assert type(created_blocklist) is dict
        for key in ['id', 'headers', 'name']:
            assert key in created_blocklist.keys()

        response = app.get('/blocklist')
        assert response.status_code == 200
        assert type(response.json) is list
        assert len(response.json) == 1
        assert response.json[0] == created_blocklist

    def test_create_blocklist_with_headers(self, app):
        """
        create blocklist with headers
        :param app:
        :return:
        """
        response = app.post(
            '/blocklist',
            json={'name': 'blah',
                  'headers': [
                      {'name': 'x-requested-with', 'value': 'com.yarr.blah'},
                      {'name': 'x-requested-with', 'value': 'com.meh.blah'}
                  ]
                  }
        )
        assert response.status_code == 200
        created_blocklist = response.json
        assert type(created_blocklist) is dict
        for key in ['id', 'headers', 'name']:
            assert key in created_blocklist.keys()

        assert len(created_blocklist['headers']) == 2

        for header_id in created_blocklist['headers']:
            response = app.get(f'/header/{header_id}')
            assert response.status_code == 200
