import pytest
from defiant import create_app


class BaseApplicationTest(object):
    @pytest.fixture()
    def app(self):
        _app = create_app()

        # i'd rather not import models.db here
        db = _app.extensions['sqlalchemy'].db
        with _app.app_context():
            db.drop_all()
            db.create_all()
            db.session.commit()
        return _app.test_client()
