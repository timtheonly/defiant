from defiant.flask_internals.configurator import Configurator
from defiant.flask_internals.route_injector import RouteInjector
from defiant.models import db, marsh
from defiant.models.header import Header
from defiant.models.blocklist import Blocklist

# import all the resources here so they register themselves with the RouteInjector
# TODO: move this to RouteInjector?
from defiant.resources import *

from flask import Flask
from flask_restful import Api


def create_app(test_config=None) -> Flask:
    app = Flask(__name__)
    config = Configurator().config

    app.config.from_object(config)
    # initialize all the flask addons
    api = Api(app, errors=config.get_config("ERRORS", {}))
    db.init_app(app)
    marsh.init_app(app)
    api = RouteInjector().bootstrap(api)
    return app
