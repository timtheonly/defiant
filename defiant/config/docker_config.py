from defiant.config.base_config import BaseConfig


class DockerConfig(BaseConfig):
    DB_HOST = "maria"
    ENV = "development"
    DEBUG = True
