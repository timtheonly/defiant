from defiant.config.base_config import BaseConfig


class UnittestConfig(BaseConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/test.db"
