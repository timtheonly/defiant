class BaseConfig(object):

    @classmethod
    def get_config(cls, name, default):
        return getattr(cls, name, default)

    DEBUG = True
    ENV = "development"
    DB_HOST = "localhost"
    DB_USER = "defiant_user"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ERRORS = {
        "ResourceNotFoundError": {
            "message": "A resource with that id does not exist",
            "status": 410
        }
    }
    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return f'mysql+pymysql://{self.DB_USER}:fujitsu@{self.DB_HOST}:3306/defiant'
