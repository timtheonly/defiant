from defiant.models import db, marsh


class BlocklistItem(db.model):
    id = db.Column(db.Integer, primary_key=True)
    blocklist_id = db.Column(db.Integer, db.ForeignKey('blocklist.id'))
    header_id = db.Column(db.Integer, db.ForeignKey('header.id'))


class BlocklistItemSchema(marsh.ModelSchema):
    class Meta:
        model = BlocklistItem
