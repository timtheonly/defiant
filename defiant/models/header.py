from defiant.models import db, marsh


class Header(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    value = db.Column(db.String(100), nullable=False)
    blocklist_id = db.Column(db.Integer, db.ForeignKey('blocklist.id'))

    def __init__(self, name: str, value: str):
        self.name = name
        self.value = value


class HeaderSchema(marsh.ModelSchema):
    class Meta:
        model = Header
