from defiant.models import db, marsh


class Blocklist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    headers = db.relationship('Header', backref='blocklist', lazy=True)


class BlocklistSchema(marsh.ModelSchema):
    class Meta:
        model = Blocklist
