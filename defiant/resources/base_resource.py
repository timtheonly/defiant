from typing import Union, List, Optional
from flask_restful import Resource


class BaseResource(Resource):
    single_item_schema = None
    multiple_item_schema = None
    model = None

    def get(self, obj_id: Optional[int] = None) -> List[Union[dict, None]]:
        data = {}
        if obj_id is not None:
            obj = self.model.query.filter_by(id=obj_id).first()
            data = self.single_item_schema.dump(obj).data
        else:
            obj_list = self.model.query.all()
            data = self.multiple_item_schema.dump(obj_list).data
        return data
