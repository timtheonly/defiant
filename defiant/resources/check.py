from defiant.flask_internals.Exceptions import ResourceNotFoundError
from defiant.flask_internals.marshal import marshal_with
from defiant.flask_internals.route_injector import register
from defiant.models.blocklist import Blocklist, BlocklistSchema
from defiant.models.header import Header, HeaderSchema
from defiant.resources.base_resource import BaseResource

from typing import List, Optional


@register(['/check', '/check/<int:blocklist_id>'])
class CheckResource(BaseResource):
    single_item_schema = BlocklistSchema()
    multiple_item_schema = BlocklistSchema(many=True)
    model = Blocklist

    def get(self, obj_id: Optional[int] = None) -> None:
        pass

    @marshal_with(HeaderSchema(many=True))
    def post(self, headers: List[Header], blocklist_id: int = None):
        if blocklist_id:
            blocklist = self.model.query.filter_by(id=blocklist_id).first()
            if not blocklist:
                raise ResourceNotFoundError  # internal error mapped to response
            for header in headers:
                for blocked_header in blocklist.headers:
                    if header.name == blocked_header.name and header.value == blocked_header.value:
                        return {"message": "Headers on blocklist"}
            return {"message": "Not on blocklist"}
