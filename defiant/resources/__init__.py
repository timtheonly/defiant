# import all Resources here that way the defiant will pick them up on init

from .header import HeaderResource
from .blocklist import BlocklistResource
from .check import CheckResource
