from defiant.models import db
from defiant.models.header import Header, HeaderSchema
from defiant.resources.base_resource import BaseResource
from defiant.flask_internals.marshal import marshal_with
from defiant.flask_internals.route_injector import register


@register(['/header', '/header/<int:obj_id>'])
class HeaderResource(BaseResource):

    model = Header
    single_item_schema = HeaderSchema()
    multiple_item_schema = HeaderSchema(many=True)

    @marshal_with(single_item_schema)
    def post(self, header: HeaderSchema) -> dict:
        db.session.add(header)
        db.session.commit()
        return self.single_item_schema.dump(header).data
