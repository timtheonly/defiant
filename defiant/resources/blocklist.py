from defiant.models import db
from defiant.models.header import HeaderSchema
from defiant.models.blocklist import Blocklist, BlocklistSchema
from defiant.resources.base_resource import BaseResource
from defiant.flask_internals.marshal import marshal_with_embedded
from defiant.flask_internals.route_injector import register


@register(['/blocklist', '/blocklist/<int:obj_id>'])
class BlocklistResource(BaseResource):

    single_item_schema = BlocklistSchema()
    multiple_item_schema = BlocklistSchema(many=True)
    model = Blocklist

    @marshal_with_embedded(single_item_schema, HeaderSchema(many=True), "headers")
    def post(self, blocklist: BlocklistSchema) -> dict:
        db.session.add(blocklist)
        db.session.commit()
        return self.single_item_schema.dump(blocklist).data

