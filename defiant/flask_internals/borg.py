class Borg(object):
    """
    borg pattern you will be assimilated
    """
    __shared_state = {}

    def __init__(self):
        self.__dict__ = self.__shared_state
