import os
from defiant.config.base_config import BaseConfig
from defiant.config.unittest_config import UnittestConfig
from defiant.config.docker_config import DockerConfig
from defiant.flask_internals.borg import Borg


class Configurator(Borg):
    config = None
    env_map = {
        "docker": DockerConfig,
        "unittest": UnittestConfig
    }

    def __init__(self):
        super().__init__()
        if self.config is None:
            # mostly for debugging, no reason why this cannot be directly assigned to self
            _config = (self.env_map.get(os.environ.get('CONFIG'), BaseConfig))()
            self.config = _config
