from flask_restful import HTTPException

class ResourceNotFoundError(HTTPException):
    pass
