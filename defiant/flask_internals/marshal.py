from flask import request
from marshmallow import ValidationError
from typing import Callable, Union
from functools import wraps
from defiant.models import marsh


def marshal_with(schema: marsh.ModelSchema) -> Callable:
    """
    Marshal given schema from the request and pass it to the func
    :param schema:
    :return:
    """
    def decorator_marshal_with(func: Callable) -> Callable:
        @wraps(func)
        def wrapper_marshal_with(self, *args, **kwargs) -> Union[marsh.ModelSchema, dict]:
            request_data = request.get_json(force=True)
            if not request_data:
                return {"error": "WTF no data"}
            try:
                obj, errors = schema.load(request_data)
            except ValidationError as e:
                # flask-marshmallow should catch these but the tests cannot so...
                obj = {}
                errors = e.messages
            if errors:
                return errors

            return func(self, obj, *args, **kwargs)

        return wrapper_marshal_with
    return decorator_marshal_with


def marshal_with_embedded(schema: marsh.ModelSchema, embedded_schema: marsh.ModelSchema, key: str):
    """
    Marshal given schema from request and parse embedded_schema on given key, pass result to func
    :param schema:
    :param embedded_schema:
    :param key:
    :return:
    """
    def decorator_marshal_with(func: Callable) -> Callable:
        @wraps(func)
        def wrapper_marshal_with(self, *args) -> Union[marsh.ModelSchema, dict]:
            request_data = request.get_json(force=True)
            if not request_data:
                return {"error": "WTF no data"}

            try:
                # parse the embedded obj first
                key_data = request_data.pop(key)
                emb_obj, errors = embedded_schema.load(key_data)
                if errors:
                    return errors

                # now parse the parent obj and append the embedded_obj
                obj, errors = schema.load(request_data)
                if errors:
                    return errors
                # in tests schema.load returns a dict
                if type(obj) == dict:
                    obj[key] = emb_obj
                else:
                    setattr(obj, key, emb_obj)
            except KeyError:
                # embedded key not present just parse the parent schema
                obj, errors = schema.load(request_data)
                if errors:
                    return errors
            except ValidationError as e:
                # flask-marshmallow should catch these but the tests cannot so...
                return e.messages

            return func(self, obj, *args)
        return wrapper_marshal_with
    return decorator_marshal_with
