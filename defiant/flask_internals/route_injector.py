from .borg import Borg
from flask_restful import Api
from typing import Callable


def register(urls: list) -> Callable:
    def decorator(cls) -> Callable:
        RouteInjector().register_route({"resource": cls, "available_routes": urls})
        return cls
    return decorator


class RouteInjector(Borg):
    __routes = []

    def __init__(self):
        super().__init__()

    def register_route(self, route_info: dict) -> None:
        self.__routes.append(route_info)

    def bootstrap(self, api: Api) -> Api:
        for route in self.__routes:
            api.add_resource(route['resource'], *route['available_routes'])
        return api
