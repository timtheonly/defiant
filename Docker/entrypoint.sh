#! /bin/sh

cd /deploy

git clone https://github.com/vishnubob/wait-for-it.git
cp wait-for-it/wait-for-it.sh .

# wait for the db to spin up first
bash ./wait-for-it.sh maria:3306

# why wipe the db all the time ?
python setup_db.py --docker --wipe

# clean up after yo self
rm -rf wait-for-it/ wait-for-it.sh

python run.py