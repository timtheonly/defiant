import os
import sys
import defiant
import argparse
from defiant.models import db

"""
IMPORTANT set FLASK_ENV to whichever environment you are  creating the BD for
"""


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--docker", action="store_true")
    parser.add_argument("--wipe", action="store_true")
    args = parser.parse_args()

    app = defiant.create_app()

    if not args.docker:
        # we're not on docker we should ask permssion
        env = os.environ.get("FLASK_ENV", "dev")
        and_wipe = " "
        if args.wipe:
            and_wipe = " and wipe "
        response = input(f"this action will create{and_wipe}DB on {env}, continue y/N?")
        if response.lower() != 'y':
            print("bailing out...")
            sys.exit()

    with app.app_context():
        if args.wipe:
            # wipe the existing DB if it exists
            db.drop_all()
        db.create_all()
        db.session.commit()


if __name__ == '__main__':
    main()
